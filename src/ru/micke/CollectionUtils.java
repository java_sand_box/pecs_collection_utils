package ru.micke;

import java.util.*;

public class CollectionUtils {
    public static<T> void addAll(List<? extends T> source, List<? super T> destination) {
        destination.addAll(source);
    }
    public static <T> List <T> newArrayList() {
        return new ArrayList<>();
    }
    public static <T> int indexOf(List < ? super T > source, T object) {
        return source.indexOf(object);
    }
    public static <T> List < T > limit(List < ? extends T > source, int size) {
        return  (source.size() < size) ? new ArrayList<>(source)
                                       : new ArrayList<>(source.subList(0, source.size() -1));
    }
    public static <T> void add(List <? super T> source, T obj) {
        source.add(obj);
    }
    public static <T> void removeAll(List <? super T> removeFrom, List <? extends T> c2) {
        c2.forEach(removeFrom::remove);
    }
//    //true если первый лист содержит все элементы второго
    public static <T> boolean containsAll(List <? super T>c1, List  <? extends T> c2) {
        for (T t : c2) {
            if (!c1.contains(t)) {
                return false;
            }
        }
        return true;
    }
    //true если первый лист содержит хотя-бы 1 второго
    public static <T> boolean containsAny(List <? super T>c1, List  <? extends T> c2) {
        for (T t : c2) {
            if (c1.contains(t)) {
                return true;
            }
        }
        return false;
    }
    //Возвращает лист, содержащий элементы из входного листа в диапазоне от min до max.
    // Элементы сравнивать через Comparable.
    // Прмер range(Arrays.asList(8,1,3,5,6, 4), 3, 6) вернет {3,4,5,6}
    public static <T extends Comparable <? super T> > List <T> range(List  <T> list, T min, T max) {
        List <T> retList = new LinkedList<>();
        for(T element:list){
            if(min.compareTo(element) <= 0 && max.compareTo(element) >= 0){
                retList.add(element);
            }
        }
        Collections.sort(retList);
        return retList;
    }
    //Возвращает лист, содержащий элементы из входного листа в диапазоне от min до max.
    // Элементы сравнивать через Comparable.
    // Прмер range(Arrays.asList(8,1,3,5,6, 4), 3, 6) вернет {3,4,5,6}
    public static <T extends Comparable<? super T>> List <T> range( List <T> list, T min, T max
                                                                  , Comparator <T> comparator) {
        List <T> retList = new LinkedList<>();
        for(T element:list){
            if(comparator.compare(min, element) <= 0 && comparator.compare(max,element) >= 0){
                retList.add(element);
            }
        }
        retList.sort(comparator);
        return retList;
    }
}