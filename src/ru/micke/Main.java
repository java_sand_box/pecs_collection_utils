package ru.micke;
import java.util.*;

import static ru.micke.CollectionUtils.range;

public class Main {

    public static void main(String[] args) {
        System.out.println("**************newArrayList*****************");
        List<Number> list =CollectionUtils.newArrayList();
        CollectionUtils.add(list,10);
        CollectionUtils.add(list,10.5);
        System.out.println(list);

        System.out.println("*******************indexOf*****************");
        System.out.println(CollectionUtils.indexOf(list,10));
        System.out.println(CollectionUtils.indexOf(list,10.5));
//        System.out.println(CollectionUtils.indexOf(list,"sdf"));
        System.out.println(CollectionUtils.indexOf(list,1));

        System.out.println("*****************limit*********************");
        List <Double> list2 = new ArrayList<>(Arrays.asList(1.,2.,3.,4.,5.,6.));
        List <Number> list3= CollectionUtils.limit(list2, 3);
        System.out.println("list3 " + list3);
        list3.add(3);
        System.out.println("list3 " + list3);

        System.out.println("*****************removeAll*****************");
        CollectionUtils.removeAll(list3,list2);
        System.out.println("list3 " +list3);

        System.out.println("*****************range*********************");
        List<Double> range = range(list2, 2., 5.);
        range.add(10.);
        System.out.println("range " + range);

        System.out.println("*****************range Comparator**********");
        List<Double> range1 = range(list2, 2., 5., Comparator.naturalOrder());
        range1.add(100.);
        System.out.println("range with comparator " + range1);
    }
}
